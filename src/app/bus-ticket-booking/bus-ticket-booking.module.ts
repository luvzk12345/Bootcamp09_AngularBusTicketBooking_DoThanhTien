import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusTicketBookingComponent } from './bus-ticket-booking.component';
import { SeatListComponent } from './seat-list/seat-list.component';
import { BookingSeatListComponent } from './booking-seat-list/booking-seat-list.component';

@NgModule({
  declarations: [
    BusTicketBookingComponent,
    SeatListComponent,
    BookingSeatListComponent,
  ],
  imports: [CommonModule],
  exports: [BusTicketBookingComponent],
})
export class BusTicketBookingModule {}
