import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seat-list',
  templateUrl: './seat-list.component.html',
  styleUrls: ['./seat-list.component.scss'],
})
export class SeatListComponent implements OnInit {
  @Input() seatList!: Seat[];
  @Input() bookingSeatList!: Seat[];
  @Output() eventSetBookingSeat = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  setBookingSeat(seat: Seat) {
    this.eventSetBookingSeat.emit(seat);
  }

  checkIsBooking(seat: Seat) {
    const foundIndex = this.bookingSeatList.findIndex(
      (item) => item.SoGhe === seat.SoGhe
    );

    return foundIndex === -1 ? false : true;
  }
}

interface Seat {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
