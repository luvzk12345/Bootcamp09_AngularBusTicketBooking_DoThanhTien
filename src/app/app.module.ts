import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BusTicketBookingModule } from './bus-ticket-booking/bus-ticket-booking.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BusTicketBookingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
